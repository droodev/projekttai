package pl.edu.agh.core.init;

import java.util.UUID;

import javax.inject.Inject;

import pl.edu.agh.persistence.UsersDAO;
import pl.edu.agh.shiro.User;

public class ServiceInitializer {
	
	private UsersDAO usersDAO;
	
	@Inject
	public ServiceInitializer(UsersDAO usersDAO) {
		this.usersDAO = usersDAO;
	}

	public void initDatabaseData(){
		usersDAO.save(new User("admin", "admin", UUID.randomUUID().toString()));
		usersDAO.save(new User("user", "user", UUID.randomUUID().toString()));
		return;
	}
}
