package pl.edu.agh.core;

import javax.inject.Inject;
import javax.servlet.ServletException;

import pl.edu.agh.core.init.ServiceInitializer;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.ServiceException;
import com.vaadin.server.SessionInitEvent;
import com.vaadin.server.SessionInitListener;
import com.vaadin.server.UIProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("mytheme")
public class MyVaadinUI extends UI {
	
	@VaadinServletConfiguration(productionMode = false, ui = MyVaadinUI.class)
	public static class Servlet extends VaadinServlet implements SessionInitListener{
		
		@Inject
		private ServiceInitializer initializer;
		
		@Inject
		private UIProvider uiProvider;
		
		@Override
		protected void servletInitialized() throws ServletException {
			getService().addSessionInitListener(this);
			super.servletInitialized();
			initializer.initDatabaseData();
		}

		@Override
		public void sessionInit(SessionInitEvent event) throws ServiceException {
			event.getSession().addUIProvider(uiProvider);
		}
		
	}

	
	@Override
	protected void init(VaadinRequest request) {
		setSizeFull();
	}

}
