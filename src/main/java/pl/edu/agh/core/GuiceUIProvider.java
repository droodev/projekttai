package pl.edu.agh.core;

import javax.inject.Inject;

import pl.edu.agh.helpers.inject.UIClass;


import com.google.inject.Provider;
import com.vaadin.server.UIClassSelectionEvent;
import com.vaadin.server.UICreateEvent;
import com.vaadin.server.UIProvider;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
public class GuiceUIProvider extends UIProvider{
	
	private Provider<UI> uiProvider;
	
	private Class<? extends UI> clazz;
	
	@Inject
	public GuiceUIProvider(Provider<UI> uiProvider, @UIClass Class<? extends UI> clazz) {
		this.uiProvider = uiProvider;
		this.clazz = clazz;
	}

	@Override
	public UI createInstance(UICreateEvent event) {
		return uiProvider.get();
	}
	
	@Override
	public Class<? extends UI> getUIClass(UIClassSelectionEvent event) {
		return clazz;
	}

}
