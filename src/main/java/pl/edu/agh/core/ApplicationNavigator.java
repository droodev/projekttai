package pl.edu.agh.core;

import javax.inject.Inject;

import pl.edu.agh.core.view.LoginView;
import pl.edu.agh.core.view.MainView;
import pl.edu.agh.core.view.UploadView;
import pl.edu.agh.dropbox.auth.DropboxAuthorizer;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.SingleComponentContainer;
import com.vaadin.ui.UI;



@SuppressWarnings("serial")
public class ApplicationNavigator extends Navigator{
	
	public static final String MAIN_V_NAME = "main";
	public static final String UPLOAD_V_NAME = "upload";
	public static final String LOGIN_V_NAME = "";
	
	@Inject
	public ApplicationNavigator(UI ui, SingleComponentContainer container, MainView mainView, UploadView uploadView, LoginView loginView, final DropboxAuthorizer dbAuthorizer) {
		super(ui, container);
		addView(LOGIN_V_NAME, loginView);
		addView(MAIN_V_NAME, mainView);
		addView(UPLOAD_V_NAME, uploadView);
		
		addViewChangeListener(new ViewChangeListener() {
			
			

			@Override
			public boolean beforeViewChange(ViewChangeEvent event) {
				if(event.getViewName().equals(MAIN_V_NAME)){
					return dbAuthorizer.authorize();
				}
				return true;
			}
			
			@Override
			public void afterViewChange(ViewChangeEvent event) {
			}
		});
	}

	
	
	
}
