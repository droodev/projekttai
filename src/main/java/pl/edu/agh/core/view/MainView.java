package pl.edu.agh.core.view;

import javax.inject.Inject;

import pl.edu.agh.core.components.DropboxTreePanel;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.HorizontalLayout;

@SuppressWarnings("serial")
public class MainView extends HorizontalLayout implements View{
	
	private DropboxTreePanel dropboxPanel;
	
	
	@Inject
	public MainView(DropboxTreePanel dropboxPanel) {
		this.dropboxPanel = dropboxPanel;
	}



	@Override
	public void enter(ViewChangeEvent event) {
		setSizeFull();
		addComponent(dropboxPanel);
		
	}
}
