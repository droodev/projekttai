package pl.edu.agh.core.view;

import javax.inject.Inject;

import pl.edu.agh.core.components.LoggingPanel;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.GridLayout;

@SuppressWarnings("serial")
public class LoginView extends GridLayout implements View{

	LoggingPanel loginComponent;
	
	
	
	@Inject
	public LoginView(LoggingPanel loginComponent) {
		this.loginComponent = loginComponent;
	}




	@Override
	public void enter(ViewChangeEvent event) {
		
		setMargin(true);
		setColumns(1);
		setRows(1);

		loginComponent.setSizeFull();
		addComponent(loginComponent, 0, 0);
		setComponentAlignment(loginComponent, Alignment.MIDDLE_CENTER);
		setSizeFull();
	}

}
