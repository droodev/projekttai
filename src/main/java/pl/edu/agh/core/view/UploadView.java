package pl.edu.agh.core.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

import javax.inject.Inject;

import pl.edu.agh.core.ApplicationNavigator;

import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxWriteMode;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.FinishedListener;
import com.vaadin.ui.VerticalLayout;


@SuppressWarnings("serial")
public class UploadView extends VerticalLayout implements View{
	
	private static final Logger logger = Logger.getLogger(UploadView.class.getName());
	
	private DbxClient client;
	private final Navigator navigator;
	
	@Inject
	public UploadView(DbxClient client,Navigator navigator) {
		this.client = client;
		this.navigator = navigator;
	}


	@Override
	public void enter(ViewChangeEvent event) {
		final SavingDbxReceiver uploadReceiver = new SavingDbxReceiver();
		Upload uploadComponent = new Upload("Upload file", uploadReceiver);
		uploadComponent.addFinishedListener(new FinishedListener() {
			
			@Override
			public void uploadFinished(FinishedEvent event) {
				logger.info(String.format("Upload finished"));
				uploading();
				logger.info(String.format("After uploading method"));
				navigator.navigateTo(ApplicationNavigator.MAIN_V_NAME);
			}

			private void uploading() {
				ByteBuffer buffer = uploadReceiver.getBuffer();
				String filename = uploadReceiver.getFilename();
				ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buffer.array(),0, buffer.position());
				try {
					client.uploadFile("/upload/" + filename, DbxWriteMode.add(), buffer.position(), byteArrayInputStream);
				} catch (DbxException e) {
					e.printStackTrace();
					System.exit(1);
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(1);
				}
			}
		});
		addComponent(uploadComponent);
		
		
	}

	
	private class SavingDbxReceiver implements Upload.Receiver{

		private ByteBuffer buffer = ByteBuffer.allocate(1024);
		private String filename;
		
		public String getFilename() {
			return filename;
		}

		@Override
		public OutputStream receiveUpload(String filename, String mimeType) {
			this.filename=filename;
			return new OutputStream() {
				@Override
				public void write(int b) throws IOException {
					buffer.put((byte)b);
				}
			};
		}

		public ByteBuffer getBuffer() {
			return buffer;
		}
		
	}
}
