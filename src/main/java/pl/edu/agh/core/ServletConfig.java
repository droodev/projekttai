package pl.edu.agh.core;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.shiro.guice.web.ShiroWebModule;

import pl.edu.agh.shiro.core.ShiroModule;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

public class ServletConfig extends GuiceServletContextListener {
	private ServletContext servletContext;

	@Override
	protected Injector getInjector() {
		return Guice.createInjector(new StandardModule(), new ShiroModule(
				servletContext), ShiroWebModule
				.guiceFilterModule());
	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		servletContext = servletContextEvent.getServletContext();
		super.contextInitialized(servletContextEvent);
	}

}
