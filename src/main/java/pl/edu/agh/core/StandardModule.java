package pl.edu.agh.core;

import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

import pl.edu.agh.core.components.DropboxTreePanel;
import pl.edu.agh.core.components.LoggingPanel;
import pl.edu.agh.core.components.tree.FilesTree;
import pl.edu.agh.core.components.tree.FilesTreeControler;
import pl.edu.agh.core.components.tree.FilesTreeModel;
import pl.edu.agh.core.init.ServiceInitializer;
import pl.edu.agh.core.view.LoginView;
import pl.edu.agh.core.view.MainView;
import pl.edu.agh.core.view.UploadView;
import pl.edu.agh.dropbox.auth.AppTokenProvider;
import pl.edu.agh.dropbox.auth.AuthorizingServlet;
import pl.edu.agh.dropbox.auth.DbxAppInfoProvider;
import pl.edu.agh.dropbox.auth.DropboxAuthorizer;
import pl.edu.agh.dropbox.auth.HardcodedAppInfoProvider;
import pl.edu.agh.dropbox.tree.DropboxTreeModel;
import pl.edu.agh.helpers.inject.ApplicationKeyDBValue;
import pl.edu.agh.helpers.inject.PassDBValue;
import pl.edu.agh.helpers.inject.UIClass;
import pl.edu.agh.persistence.ApplicationKeyDAO;
import pl.edu.agh.persistence.UsersDAO;
import pl.edu.agh.persistence.init.DatabaseMechanismsIntializer;

import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxRequestConfig;
import com.google.code.morphia.Datastore;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import com.google.inject.servlet.ServletModule;
import com.google.inject.servlet.SessionScoped;
import com.mongodb.MongoClient;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.UIProvider;
import com.vaadin.ui.SingleComponentContainer;
import com.vaadin.ui.Tree;
import com.vaadin.ui.UI;

public class StandardModule extends ServletModule {
	@Override
	protected void configureServlets(){
		bindApplicationCore();
		bindPersistence();
		

		bind(MyVaadinUI.class).in(SessionScoped.class);
		bind(SingleComponentContainer.class).to(MyVaadinUI.class);
		bind(UI.class).to(MyVaadinUI.class);
		
		bind(Navigator.class).to(ApplicationNavigator.class).in(SessionScoped.class);
		
		bind(new TypeLiteral<Class<? extends UI>>() {
		}).annotatedWith(UIClass.class).toInstance(MyVaadinUI.class);
		bind(UIProvider.class).to(GuiceUIProvider.class);
		bind(LoggingPanel.class);
		bind(LoginView.class).in(SessionScoped.class);
		bind(UploadView.class).in(SessionScoped.class);
		bind(MainView.class).in(SessionScoped.class);
		
		bind(AppTokenProvider.class).in(Singleton.class);
		bind(DbxAppInfoProvider.class).to(HardcodedAppInfoProvider.class).in(Singleton.class);
		
		bind(FilesTreeControler.class);
		bind(Tree.class).to(FilesTree.class);
		
		bind(FilesTreeModel.class).to(DropboxTreeModel.class);
		
		bind(DropboxTreePanel.class);
		
	}

	private void bindPersistence() {
		bind(String.class).annotatedWith(PassDBValue.class).toInstance(
				"Credentials");
		bind(String.class).annotatedWith(ApplicationKeyDBValue.class)
				.toInstance("ApplicationKey");

		bind(Datastore.class).annotatedWith(PassDBValue.class).toProvider(
				DatabaseMechanismsIntializer.PassDatastoreProvider.class).in(Singleton.class);
		bind(Datastore.class)
				.annotatedWith(ApplicationKeyDBValue.class)
				.toProvider(
						DatabaseMechanismsIntializer.ApplicationKeyDatastoreProvider.class).in(Singleton.class);
		bind(UsersDAO.class).in(Singleton.class);
		bind(ApplicationKeyDAO.class).in(Singleton.class);
		bind(ServiceInitializer.class).in(Singleton.class);
	}

	private void bindApplicationCore() {
		bindAuthorizer();
		bind(MongoClient.class).toProvider(DatabaseMechanismsIntializer.class);
		bind(MyVaadinUI.Servlet.class).in(Singleton.class);
		serve("/*").with(MyVaadinUI.Servlet.class);
	}
	
	private void bindAuthorizer(){
		bind(AuthorizingServlet.class).in(Singleton.class);
		serve("/appAuth").with(AuthorizingServlet.class);
		bind(DropboxAuthorizer.class).in(Singleton.class);
	}

	
	@Provides
	@Singleton
	@Inject
	public DbxClient clientProvider(AppTokenProvider tokenProvider){
		return new DbxClient(new DbxRequestConfig("CzarPrys", Locale.getDefault().toString()), tokenProvider.getAppToken(null));
	}
}
