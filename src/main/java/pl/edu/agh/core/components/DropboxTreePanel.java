package pl.edu.agh.core.components;

import javax.inject.Inject;

import pl.edu.agh.core.ApplicationNavigator;
import pl.edu.agh.core.components.tree.FilesTree;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;

public class DropboxTreePanel extends CustomComponent  {

	private static final long serialVersionUID = 1L;
	
	private Button addNewFileButton;
	
	private Navigator navigator;
	
	

	@Inject
	public DropboxTreePanel(Navigator navigator) {
		this.navigator = navigator;
	}

	@SuppressWarnings("serial")
	void initComponents()
	{
		Button addButton = new Button("add new file if you're an admin");
		addButton.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				navigator.navigateTo(ApplicationNavigator.UPLOAD_V_NAME);
			}
		});
		addNewFileButton = addButton;
	}
	
	@Inject
	public DropboxTreePanel(FilesTree filesTree) {
		initComponents();
		final VerticalLayout layout = new VerticalLayout();
		
		layout.addComponent(filesTree);
		layout.addComponent(addNewFileButton);
		setCompositionRoot(layout);
	}

}
