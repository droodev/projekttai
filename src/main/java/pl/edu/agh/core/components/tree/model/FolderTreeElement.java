package pl.edu.agh.core.components.tree.model;

import java.util.List;

public final class FolderTreeElement extends TreeElement{
	
	public static final FolderTreeElement getRootFolder(List<TreeElement> children){
		return new FolderTreeElement("/", children, "/");
	}

	public static final FolderTreeElement getFolder(String name, List<TreeElement> children, String path){
		return new FolderTreeElement(name, children, path);
	}
	
	private FolderTreeElement(String name, List<TreeElement> children, String path) {
		super(name, children, path);
	}


	@Override
	public boolean isFolder() {
		return true;
	}

}
