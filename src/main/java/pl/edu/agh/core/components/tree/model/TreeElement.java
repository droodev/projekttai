package pl.edu.agh.core.components.tree.model;

import java.util.LinkedList;
import java.util.List;

import com.google.common.collect.ImmutableList;

public abstract class TreeElement {

	private String name;
	private LinkedList<TreeElement> children ;
	private String path;


	protected TreeElement(String name, List<TreeElement> children, String path) {
		this.name = name;
		this.children = new LinkedList<>(children);
		this.path = path;
	}

	@Override
	public String toString() {
		return name;
	}

	public List<TreeElement> getChildren(){
		return ImmutableList.copyOf(children);
	}

	public String getPath() {
		return path;
	}

	public abstract boolean isFolder();

}
