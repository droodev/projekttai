package pl.edu.agh.core.components.tree;

import java.util.logging.Logger;

import pl.edu.agh.core.components.tree.model.TreeElement;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Tree.CollapseEvent;
import com.vaadin.ui.Tree.CollapseListener;
import com.vaadin.ui.Tree.ExpandEvent;
import com.vaadin.ui.Tree.ExpandListener;

public class FilesTreeControler implements CollapseListener, ExpandListener, ItemClickListener {
	
	private static final Logger logger = Logger.getLogger(FilesTreeControler.class.getName());
	
	private static final long serialVersionUID = 1L;
	private FilesTree tree;

	public void startControlling(FilesTree tree) {
		this.tree=tree;
		tree.addExpandListener(this);
		tree.addCollapseListener(this);
		tree.addItemClickListener(this);
	}

	@Override
	public void nodeExpand(ExpandEvent event) {
		TreeElement parent = (TreeElement) event.getItemId();
		tree.nodeUpdated(parent);
	}

	@Override
	public void nodeCollapse(CollapseEvent event) {
		TreeElement parent = (TreeElement) event.getItemId();
		tree.childrenDeletion(parent);
	}

	@Override
	public void itemClick(ItemClickEvent event) {
		TreeElement element = (TreeElement) event.getItemId();
		logger.info(String.format("Current path: %s" ,element.getPath()));
	}	

}
