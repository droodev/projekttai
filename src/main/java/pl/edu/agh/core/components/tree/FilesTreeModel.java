package pl.edu.agh.core.components.tree;

import java.util.LinkedList;

import pl.edu.agh.core.components.tree.model.TreeElement;

public interface FilesTreeModel {
	public TreeElement getRootElement();
	public LinkedList<TreeElement> getChildrenOf(String path);
}
