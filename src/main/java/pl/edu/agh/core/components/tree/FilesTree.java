package pl.edu.agh.core.components.tree;

import java.util.Collection;
import java.util.LinkedList;

import javax.inject.Inject;

import pl.edu.agh.core.components.tree.model.TreeElement;

import com.vaadin.ui.Tree;

@SuppressWarnings("serial")
public class FilesTree extends Tree{

	private FilesTreeModel treeModel;
	
	@Inject
	public FilesTree(FilesTreeModel treeModel, FilesTreeControler controler) {
		this.treeModel = treeModel;
		controler.startControlling(this);
		fillTree();
	}

	public FilesTreeModel getTreeModel() {
		return treeModel;
	}

	private void fillTree() {
		TreeElement root = treeModel.getRootElement();
		addItem(root);
	}
	
	public void childrenDeletion(TreeElement parent){
			removeRecursively(parent);
	}
	
	public void nodeUpdated(TreeElement updated){
		LinkedList<TreeElement> newElements = treeModel.getChildrenOf(updated.getPath());
		

		for (TreeElement element : newElements) {
			addItem(element);
			setParent(element, updated);
			setChildrenAllowed(element, element.isFolder());
		}
	}
	
	private void removeRecursively(TreeElement element) {
		Collection<?> children = getChildren(element);
		if (children == null)
			return;
		Object childrenArray[] = children.toArray();
		for (Object child : childrenArray) {
			TreeElement item = (TreeElement) child;
			removeRecursively(item);
			removeItem(item);
		}

	}

}

//XXX moje wyobrazenie
/**-
 * poczatek[View]: model.getRoot
 * klikniecie[Controller]: view.updateYourselfAtPath(...)
 * 	[View]: model.getPath --- *Opcja
 * 
 * *ALE DOCELOWO - dostaniesz jeden mądry TreeElement
 * 
 * view.update -- to będzie od modelu, jeśli będzie trzeba wyświetlić model raz jeszcze(ewentulanie tylko ze ścieżką)
 */
