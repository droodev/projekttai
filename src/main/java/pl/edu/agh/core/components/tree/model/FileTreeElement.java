package pl.edu.agh.core.components.tree.model;

import java.util.LinkedList;

public final class FileTreeElement extends TreeElement{
	
	public static final FileTreeElement getFileElement(String name, String path){
		return new FileTreeElement(name, path);
	}
	
	private FileTreeElement(String name, String path) {
		super(name, new LinkedList<TreeElement>(), path);
	}

	@Override
	public boolean isFolder() {
		return false;
	}

}
