package pl.edu.agh.core.components;

import javax.inject.Inject;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import pl.edu.agh.core.ApplicationNavigator;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.VerticalLayout;

public class LoggingPanel extends CustomComponent {


	private static final long serialVersionUID = 1L;

	private ComboBox userTypeComboBox;
	private PasswordField userPassword;
	private Button loginButton;
	
	private Navigator navigator;

	@Inject
	public LoggingPanel(Navigator navigator) {
		this.navigator = navigator;
	}

	private void initComponents() {

		userTypeComboBox = new ComboBox();
		userTypeComboBox.addItem("user");
		userTypeComboBox.addItem("admin");
		userTypeComboBox.setNullSelectionAllowed(false);
		userTypeComboBox.setValue("user");

		userPassword = new PasswordField();
		loginButton = new Button("Login");

	}

	public LoggingPanel() {

		initComponents();

		Label loginLabel = new Label("user:");
		Label passwordLabel = new Label("password:");

		loginButton.addClickListener(new Button.ClickListener() {

			private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) {

				UsernamePasswordToken token = new UsernamePasswordToken(
						userTypeComboBox.getValue().toString(), userPassword
								.getValue());
				Subject currentUser = SecurityUtils.getSubject();
				try {

					currentUser.login(token);
					navigator.navigateTo(ApplicationNavigator.MAIN_V_NAME);

				} catch (UnknownAccountException uae) {
					System.out.println("username wasn't in the system");
				} catch (IncorrectCredentialsException ice) {
					System.out.println("password didn't match, try again");
				} catch (LockedAccountException lae) {
					System.out
							.println("account for that username is locked - can't login");
				} catch (AuthenticationException ae) {
					ae.printStackTrace();
				}
			}

		});

		final VerticalLayout layout = new VerticalLayout();
		layout.addComponent(loginLabel);
		layout.addComponent(userTypeComboBox);
		layout.addComponent(passwordLabel);
		layout.addComponent(userPassword);
		layout.addComponent(loginButton);

		setCompositionRoot(layout);

	}

}
