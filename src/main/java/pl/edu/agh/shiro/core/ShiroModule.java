package pl.edu.agh.shiro.core;

import javax.inject.Singleton;
import javax.servlet.ServletContext;

import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.guice.web.ShiroWebModule;

import pl.edu.agh.shiro.MongoDBRealm;


import com.google.inject.Provides;

public class ShiroModule extends ShiroWebModule{

	public ShiroModule(ServletContext servletContext) {
		super(servletContext);
	}

	@Override
	protected void configureShiroWeb() {
		try {
			bindRealm().to(MongoDBRealm.class).in(Singleton.class);
		} catch (Exception e) {
			addError(e);
		}
		
	}

	@Provides
	public CredentialsMatcher getHashedCredentialMatcher(){
		HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher(Sha256Hash.ALGORITHM_NAME);
		hashedCredentialsMatcher.setHashIterations(1024);
		hashedCredentialsMatcher.setStoredCredentialsHexEncoded(false);
		return hashedCredentialsMatcher;
	}
	
}
