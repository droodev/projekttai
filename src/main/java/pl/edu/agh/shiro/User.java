package pl.edu.agh.shiro;

import org.apache.shiro.crypto.hash.Sha256Hash;

import com.google.code.morphia.annotations.Id;

import pl.edu.agh.helpers.MorphiaMappingRequirement;

public class User {
	
	@Id
	private String login;
	private String password;
	private String salt;

	@MorphiaMappingRequirement
	private User() {
	}

	public User(String login, String password, String salt) {
		this.password = new Sha256Hash(password, salt, 1024).toBase64();
		this.login = login;
		this.salt = salt;
	}

	public String getPassword() {
		return password;
	}

	public String getLogin() {
		return login;
	}

	public String getSalt() {
		return salt;
	}

}
