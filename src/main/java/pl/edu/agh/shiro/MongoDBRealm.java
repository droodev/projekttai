package pl.edu.agh.shiro;

import javax.inject.Inject;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.ByteSource;

import pl.edu.agh.persistence.UsersDAO;

public class MongoDBRealm extends AuthenticatingRealm {

	private UsersDAO usersDAO;
	
	
	@Inject
	public MongoDBRealm(UsersDAO usersDAO, CredentialsMatcher matcher) {
		super(matcher);
		this.usersDAO = usersDAO;
	}



	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken arg0) throws AuthenticationException {
		SimpleAuthenticationInfo resultInfo = new SimpleAuthenticationInfo();
		
		User user = usersDAO.get(arg0.getPrincipal().toString());
				
		if (user == null) {
			return null;
		}
		
		resultInfo.setCredentialsSalt(ByteSource.Util.bytes(user.getSalt()));

		SimplePrincipalCollection principalCollection = new SimplePrincipalCollection();
		principalCollection.add(user.getLogin(), getName());
		resultInfo.setPrincipals(principalCollection);

		resultInfo.setCredentials(user.getPassword());
		
		return resultInfo;
	}

}
