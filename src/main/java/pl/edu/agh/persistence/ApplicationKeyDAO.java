package pl.edu.agh.persistence;

import javax.inject.Inject;

import pl.edu.agh.dropbox.auth.ApplicationKey;
import pl.edu.agh.helpers.inject.ApplicationKeyDBValue;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.dao.BasicDAO;

public class ApplicationKeyDAO extends BasicDAO<ApplicationKey, String>{

	@Inject
	protected ApplicationKeyDAO(@ApplicationKeyDBValue Datastore ds) {
		super(ds);
	}
	
}
