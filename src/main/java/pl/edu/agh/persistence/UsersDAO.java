package pl.edu.agh.persistence;


import javax.inject.Inject;

import pl.edu.agh.helpers.inject.PassDBValue;
import pl.edu.agh.shiro.User;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.dao.BasicDAO;

public class UsersDAO extends BasicDAO<User, String>{

	@Inject
	protected UsersDAO(@PassDBValue Datastore ds) {
		super(ds);
	}

}
