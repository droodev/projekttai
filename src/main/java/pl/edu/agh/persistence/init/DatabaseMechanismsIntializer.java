package pl.edu.agh.persistence.init;

import java.net.UnknownHostException;

import javax.inject.Inject;

import pl.edu.agh.helpers.inject.ApplicationKeyDBValue;
import pl.edu.agh.helpers.inject.PassDBValue;


import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;
import com.google.inject.Provider;
import com.mongodb.MongoClient;

public class DatabaseMechanismsIntializer implements Provider<MongoClient>{
	
	private MongoClient client;
	
	public void init(){
		try {
			client = new MongoClient();
		} catch (UnknownHostException e) {
			System.out.println("Cannot connect to DB!");
			System.exit(1);
		}
	}

	@Override
	public MongoClient get() {
		if(client==null){
			init();
		}
		return client;
	}

	public static class PassDatastoreProvider implements Provider<Datastore>{
		
		private MongoClient mongoClient;
		private String databaseName;
		
		
		@Inject
		public PassDatastoreProvider(MongoClient mongoClient, @PassDBValue String databaseName) {
			this.mongoClient = mongoClient;
			this.databaseName = databaseName;
		}

		@Override
		public Datastore get() {
			return new Morphia().createDatastore(mongoClient, databaseName);
		}
		
	}
	
	public static class ApplicationKeyDatastoreProvider implements Provider<Datastore>{
		
		private MongoClient mongoClient;
		private String databaseName;
		
		
		@Inject
		public ApplicationKeyDatastoreProvider(MongoClient mongoClient, @ApplicationKeyDBValue String databaseName) {
			this.mongoClient = mongoClient;
			this.databaseName = databaseName;
		}

		@Override
		public Datastore get() {
			return new Morphia().createDatastore(mongoClient, databaseName);
		}
		
	}
	
}
