package pl.edu.agh.dropbox.auth;

import pl.edu.agh.helpers.MorphiaMappingRequirement;

import com.google.code.morphia.annotations.Id;

public class ApplicationKey {
	@Id
	private String key;

	@MorphiaMappingRequirement
	private ApplicationKey(){
	}
	
	public ApplicationKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}
	
	
}
