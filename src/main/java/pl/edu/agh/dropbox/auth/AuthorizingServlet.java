package pl.edu.agh.dropbox.auth;

import java.io.IOException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.edu.agh.persistence.ApplicationKeyDAO;

import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxStandardSessionStore;
import com.dropbox.core.DbxWebAuth;
import com.google.inject.Provider;

@SuppressWarnings("serial")
public class AuthorizingServlet extends HttpServlet {

	private final static Logger logger = Logger
			.getLogger(AuthorizingServlet.class.getName());

	private DbxWebAuth webAuth;
	private Provider<ApplicationKeyDAO> appKeyDAOProvider;
	private DbxAppInfoProvider appInfoProvier;

	
	@Inject
	public AuthorizingServlet(Provider<ApplicationKeyDAO> appKeyDAOProvider,
			DbxAppInfoProvider appInfoProvier) {
		this.appKeyDAOProvider = appKeyDAOProvider;
		this.appInfoProvier = appInfoProvier;
	}



	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		logger.log(Level.INFO, "Get got");
		if (req.getParameter("state") == null) {
			logger.log(Level.INFO, "Logging to dropbox");
			DbxRequestConfig config = new DbxRequestConfig("CzarPrys", Locale
					.getDefault().toString());
			DbxStandardSessionStore store = new DbxStandardSessionStore(req.getSession(),
					"dropbox-auth-csrf-token");
			webAuth = new DbxWebAuth(config,
					appInfoProvier.getDbxAppInfo(),
					"http://localhost:8080/projekt-tai/appAuth", store);
			String authorizeUrl = webAuth.start();
			resp.sendRedirect(authorizeUrl);
			return;
		}

		logger.log(Level.INFO, "Returned from dropox");
		try {
			DbxAuthFinish authFinish = webAuth.finish(
					req.getParameterMap());
			appKeyDAOProvider.get().save(
					new ApplicationKey(authFinish.accessToken));
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.log(Level.INFO, "Redirecting");
		resp.sendRedirect("http://localhost:8080/projekt-tai/#!main");
	}


	
	
//	@Override
//	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
//			throws ServletException, IOException {
//		logger.log(Level.INFO, "doPost");
//	}

}
