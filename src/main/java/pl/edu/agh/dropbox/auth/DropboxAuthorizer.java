package pl.edu.agh.dropbox.auth;

import javax.inject.Inject;

import pl.edu.agh.persistence.ApplicationKeyDAO;

import com.google.code.morphia.query.QueryResults;
import com.vaadin.ui.UI;

public class DropboxAuthorizer {

	private ApplicationKeyDAO keyApplicationDAO;

	@Inject
	public DropboxAuthorizer(ApplicationKeyDAO keyApplicationDAO) {
		this.keyApplicationDAO = keyApplicationDAO;
	}

	public boolean authorize() {
		QueryResults<ApplicationKey> applicationKeyFindingResult = keyApplicationDAO
				.find();
		if (applicationKeyFindingResult.countAll() == 1) {
			return true;
		}
		UI.getCurrent().getPage().open("/projekt-tai/appAuth", "_self");
		return false;
	}


}
