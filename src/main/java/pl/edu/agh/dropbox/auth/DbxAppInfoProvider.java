package pl.edu.agh.dropbox.auth;

import com.dropbox.core.DbxAppInfo;

public interface DbxAppInfoProvider {
	DbxAppInfo getDbxAppInfo();
}
