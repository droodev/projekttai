package pl.edu.agh.dropbox.auth;

import com.dropbox.core.DbxAppInfo;

public class HardcodedAppInfoProvider implements DbxAppInfoProvider{

	private static final String APP_KEY = "38cqssb4ru4aq34";
    private static final String APP_SECRET = "t1qo7ef07tc3kju";

	
	@Override
	public DbxAppInfo getDbxAppInfo() {
		return new DbxAppInfo(APP_KEY, APP_SECRET);
	}
	
}
