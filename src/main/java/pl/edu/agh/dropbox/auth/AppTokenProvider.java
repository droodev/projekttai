package pl.edu.agh.dropbox.auth;

import javax.inject.Inject;

import com.google.code.morphia.query.QueryResults;

import pl.edu.agh.persistence.ApplicationKeyDAO;

public class AppTokenProvider {
	
	private ApplicationKeyDAO applicationKeyDAO;

	@Inject
	public AppTokenProvider(ApplicationKeyDAO applicationKeyDAO) {
		this.applicationKeyDAO = applicationKeyDAO;
	}
		
	//Temporarily param not used, because on app only
	public String getAppToken(String app){
		QueryResults<ApplicationKey> applicationKeyFindingResult = applicationKeyDAO.find();
		if(applicationKeyFindingResult.countAll() == 1){
			return applicationKeyFindingResult.iterator().next().getKey();
		}
		throw new IllegalStateException();
	}
	
	
}
