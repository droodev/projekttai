package pl.edu.agh.dropbox.tree;

import java.util.ArrayList;
import java.util.LinkedList;

import javax.inject.Inject;

import pl.edu.agh.core.components.tree.FilesTreeModel;
import pl.edu.agh.core.components.tree.model.FileTreeElement;
import pl.edu.agh.core.components.tree.model.FolderTreeElement;
import pl.edu.agh.core.components.tree.model.TreeElement;

import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxEntry.WithChildren;
import com.dropbox.core.DbxException;

public class DropboxTreeModel implements FilesTreeModel {
	
	DbxClient client;
	
	@Inject
	public DropboxTreeModel(DbxClient client) {
		this.client = client;
	}

	@Override
	public TreeElement getRootElement() {
		return FolderTreeElement.getRootFolder(new ArrayList<TreeElement>());
	}

	@Override
	public LinkedList<TreeElement> getChildrenOf(String path) {
		LinkedList<TreeElement> toReturn = new LinkedList<TreeElement>();
		try {
			WithChildren metadataWithChildren = client.getMetadataWithChildren(path);
			for(DbxEntry child: metadataWithChildren.children){
				if(child.isFolder()){
					toReturn.add(FolderTreeElement.getFolder(child.name, new ArrayList<TreeElement>() , child.path));
				}else{
					toReturn.add(FileTreeElement.getFileElement(child.name, child.path));
				}
			}
			return toReturn;
		} catch (DbxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


}
